namespace wfaImageSelectInGrid
{
    public partial class Form1 : Form
    {
        private Bitmap b;
        private Graphics g;
        private int cellWidth;
        private int cellHeight;
        private Point startPoint;
        private Point curPoint = new(0,0);
        private int curRow;
        private int curCol;
        private int selRow;
        private int selCol;
        private (int row, int col) selBegin;
        private (int row, int col) selEnd;
        private (int row, int col) selBeginR;
        private (int row, int col) selEndR;
        private SelMode selMode = SelMode.Rectangle;

        enum SelMode
        {
            Rectangle,
            Square,
            Line
        }

        public int Cols { get; private set; } = 7;
        public int Rows { get; private set; } = 5;


        public Form1()
        {
            InitializeComponent();
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b,curPoint);
            this.Resize += (s, e) => ResizeCells();
            ResizeCells();
            this.MouseDown += Form1_MouseDown;
            this.MouseMove += Form1_MouseMove;
            this.MouseUp += Form1_MouseUp;
            this.KeyDown += Form1_KeyDown;
            this.KeyUp += Form1_KeyUp;
            this.Text += ":(Shift+LMouse - ���, Alt+LMouse -  �������, CTRL+LMouse -������� )";
        }

        private void Form1_KeyUp(object? sender, KeyEventArgs e)
        {
          
        }

        private void Form1_KeyDown(object? sender, KeyEventArgs e)
        {
            selMode = e.Shift ? SelMode.Line : e.Alt ?  SelMode.Square : SelMode.Rectangle;
            //string asdasdas = selMode.ToString();
        }

        private void Form1_MouseUp(object? sender, MouseEventArgs e)
        {
            selMode = SelMode.Rectangle;
        }

        private void Form1_MouseMove(object? sender, MouseEventArgs e)
        {
            for (int i = 0; i < Rows; curRow = i, i++)
            {
                if (i * cellHeight > e.Y - curPoint.Y)
                    break;
            }
            for (int i = 0; i < Cols;curCol =i, i++)
            {
                if (i*cellWidth>e.X-curPoint.X)
                {
                    break;
                }
            }

            switch (e.Button)
            {
                case MouseButtons.Left:
                    selEnd = (row: curRow, col: curCol);
                    break;
                case MouseButtons.Right:
                    curPoint.X += e.X - startPoint.X;
                    curPoint.Y += e.Y - startPoint.Y;
                    startPoint = e.Location;
                    break;
            }
                    UpdateSelect();

           
            DrawCells();
        }

        private void Form1_MouseDown(object? sender, MouseEventArgs e)
        {
            startPoint = e.Location;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    selBegin = selEnd = (row: curRow, col: curCol);

                    UpdateSelect();
                    switch (selMode)
                    {
                        case SelMode.Rectangle:
                            break;
                        case SelMode.Square:
                            break;
                        case SelMode.Line:
                            if (Math.Abs(selBegin.row - selEnd.row) > Math.Abs(selBegin.col - selEnd.col))
                                selBeginR.col = selEndR.col = selBegin.col;
                            else
                                selBeginR.row = selEndR.row = selBegin.row;
                            break;
                        default:
                            break;
                    }
                    DrawCells();
                    break;
               
            }
        }

        private void UpdateSelect()
        {
            selBeginR = (row: Math.Min(selBegin.row, selEnd.row), col:Math.Min(selBegin.col,selEnd.col)); 
            selEndR =   (row: Math.Max(selBegin.row, selEnd.row), col:Math.Max(selBegin.col,selEnd.col));
            this.Text = $" {selBeginR}:{selEndR}";
        }

        private void ResizeCells()
        {
            cellWidth = this.ClientSize.Width / Cols;
            cellHeight = this.ClientSize.Height / Rows;
            DrawCells();
        }

        private void DrawCells()
        {
            g.Clear(DefaultBackColor);
            //
          
            g.FillRectangle(Brushes.LightBlue,
                selBeginR.col * cellWidth,
                selBeginR.row * cellHeight,
                (selEndR.col-selBeginR.col+1) *cellWidth,
                 (selEndR.row - selBeginR.row + 1) *cellHeight
                ); ;
            //

            for (int i = 0; i <= Rows; i++)
            {
                g.DrawLine(Pens.Green, 0, i * cellHeight, Cols * cellWidth, i * cellHeight);
            }
            for (int i = 0; i <= Cols; i++)
            {
                g.DrawLine(Pens.Green, i*cellWidth, 0, i*cellWidth , Rows * cellHeight);
            }

            //cursor
            g.DrawRectangle(new Pen(Color.Coral,5),
                curCol*cellWidth,
                curRow*cellHeight,
                cellWidth, cellHeight
                );
            //\
            g.DrawString($"[{curRow}:{curRow}]",
                new Font("",20),
                Brushes.Black,
                new Rectangle(curCol * cellWidth,
                curRow * cellHeight,
                cellWidth, cellHeight),
                new StringFormat { Alignment = StringAlignment.Center, LineAlignment= StringAlignment.Center}
                );
            this.Invalidate();
           
        }
    }
}