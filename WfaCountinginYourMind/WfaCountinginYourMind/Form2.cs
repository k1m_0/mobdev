﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaCountinginYourMind
{
    public partial class Form2 : Form
    {
        Random rnd = new();
        public Form2()
        {
            InitializeComponent();
            Play();
        }
        int x, y, z;

        private void button2_Click(object sender, EventArgs e)
        {
            if (z == 1)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void Play()
        {
            x = rnd.Next(20)-10;
            y = rnd.Next(20)-10;
            z = rnd.Next(2);
            this.Text = $"{z}";
            if (z==1)
            {
                z = z + rnd.Next(10) - 5;
            }
            label1.Text = $"{x}+{y}={x+y+z}";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (z == 1)
            {
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}
