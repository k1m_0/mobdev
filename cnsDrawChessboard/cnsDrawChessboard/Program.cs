﻿using System;

namespace cnsDrawChessboard
{
    class Program
    {
       
        static void Main(string[] args)
        {
            int dlina = 5, visota = 3; // габариты доски
            Console.Write(new String(' ', dlina / 2+1));
            for (int j = 0; j < 8; j++)
            {
                
                Console.Write(System.Convert.ToChar(1072+j));
                Console.Write(new String(' ', dlina - 1));
            }
            for (int i = 0; i < 8; i++)
            {
              
                for (int m = 0; m < visota; m++)
                {
                  
                    Console.WriteLine();
                    Console.ResetColor();
                    Console.Write(m== visota / 2? Convert.ToString(i+1): " ");
                    for (int j = 0; j < 8; j++)
                    {
                        Console.BackgroundColor = (i + j) % 2 != 0 ? ConsoleColor.Black : ConsoleColor.White;
                        Console.Write(new String(' ', dlina));
                    }

                }
            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ReadKey(true);
        }
    }
}
