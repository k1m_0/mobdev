namespace wfaImageScroll
{
    public partial class Form1 : Form
    {
        private Bitmap c;
        private Point currPoint = new Point(0,0);
        private Point startPoint;
        public int NumImage { get; private set; }=0;
        public int ZoomDelta { get; private set; }=100;
        public Form1()
        {
            InitializeComponent();
            loadNextImg();
            pictureBox1.Paint += (s, e) => e.Graphics.DrawImage(c, currPoint);
            pictureBox1.MouseDown += (s, e) => startPoint = e.Location;
            pictureBox1.MouseMove += PictureBox1_MouseMove;
            pictureBox1.MouseWheel += PictureBox1_MouseWheel;
            this.KeyDown += Form1_KeyDown;
        }

        private void Form1_KeyDown(object? sender, KeyEventArgs e)
        {

            if (e.KeyData == Keys.F1)
            {
                loadNextImg();
            }
        }

        private void PictureBox1_MouseWheel(object? sender, MouseEventArgs e)
        {
            this.Text = $"Delta={e.Delta}";
            ZoomDelta += e.Delta < 0 ? 2 : -2;
            refreshStatus();
        }

        private void loadNextImg()
        {
            NumImage = ++NumImage % 2;
            pictureBox1.Invalidate();
            switch (NumImage)
            {
                case 0:
                    c = new Bitmap(Properties.Resources.john);
                    break;
                case 1:
                    c = new Bitmap(Properties.Resources.fan);
                    break;
                default:
                    break;
            }
            
        }

        private void PictureBox1_MouseMove(object? sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                currPoint.X += e.X - startPoint.X;
                currPoint.Y += e.Y - startPoint.Y;
                startPoint = e.Location;
                pictureBox1.Invalidate();
            }
            refreshStatus();
        }

        private void refreshStatus()
        {
            laStatus.Text = $"NumImg={NumImage}startPoint={startPoint},currPoint={currPoint}";
        }
    }
}