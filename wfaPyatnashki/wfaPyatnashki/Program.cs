﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfaPyatnashki
{
    internal static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        /// 
        public static NewGameForm NewGameForm;
        public static Form2 Form2;

        [STAThread]
        static void Main()
        {
           

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
           Program.NewGameForm = new NewGameForm();
           Program.Form2 = new Form2();
            Application.Run(new GameForm());
        }
    }
}
