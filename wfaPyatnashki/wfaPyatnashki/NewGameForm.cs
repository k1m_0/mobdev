﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfaPyatnashki
{
    public partial class NewGameForm : Form
    {
        private static Timer timer;
        public bool isResult=false;
        public NewGameForm()
        {
            InitializeComponent();
            radioButton1.Checked = true;
        }
        class TimerState
        {
            public int Counter;
        }
        private void btn_start_Click(object sender, EventArgs e)
        {
          

            isResult = true;
            this.Hide();



        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void NewGameForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isResult)
            {
                isResult=false;
            }
            else
                Application.Exit();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                VisitLink();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to open link that was clicked.");
            }
        }

        private void VisitLink()
        {
            // Change the color of the link text by setting LinkVisited
            // to true.
            linkLabel1.LinkVisited = true;
            //Call the Process.Start method to open the default browser
            //with a URL:
            System.Diagnostics.Process.Start("https://docs.google.com/document/d/1lewI8OsVYZcPQhEzx-PQbcO9vyacZQA-oZNOC7xnhpI/edit");
        }

    }
}
