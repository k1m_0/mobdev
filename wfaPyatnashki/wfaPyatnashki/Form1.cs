﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfaPyatnashki
{
    public partial class GameForm : Form
    {
        PictureBox[,] pic1= new PictureBox[5,5];
        int[,] matrix = new int[5, 5];
        int l;
        int _x;
        int _y;
        int n = 1;
        int hodCount = 0;
        bool gameBegin = false;
        public GameForm()
        {
            InitializeComponent();
          
            Program.NewGameForm.ShowDialog();
           
            if (Program.NewGameForm.radioButton1.Checked)
            {
                n = 3;
                l = 2;
            }
            else if (Program.NewGameForm.radioButton2.Checked)
            {
                n = 4;
                l = 3;
            }
            else if (Program.NewGameForm.radioButton3.Checked)
            {
                n = 5;
                l = 4;
            }

            this.KeyDown += GameForm_KeyDown;
            FillMatrix(n);
            gameStart();
            gameBegin = true;
            DrawCellPyt(n);
        }

        private void GameForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    Hod(_x, _y + 1);
                    break;
                case Keys.Right:
                    Hod(_x, _y - 1);
                    break;
                case Keys.Up:
                    Hod(_x+1, _y);
                    break;
                case Keys.Down:
                    Hod(_x-1, _y);
                    break;
            }
        }

        void gameStart()
        {
            Random rnd = new Random();
            int tX=0, tY=0;
            int value = rnd.Next(12);
            
            for (int i=0; i<value; i++)
            {
                tX = rnd.Next(n);
                tY = rnd.Next(n);
                Hod(tX, _y);
                Hod(_x, tY);

            }
        }



        void DrawCellPyt(int n)
        {
            tableLayoutPanel1.RowCount = n;
            tableLayoutPanel1.ColumnCount = n;
            int k = 0;


            ShowMatrix(n);
        }
      


        void ShowMatrix(int n)
        {
            string str = $"C:/workV/mobDev2/wfaPyatnashki/img/"; ;
            string format = $"png"; ;
            if ((Program.NewGameForm.radioButton2.Checked || Program.NewGameForm.radioButton1.Checked))
            {

                str = $"C:/workV/mobDev2/wfaPyatnashki/img/";
                format = $"png";
            }
            else if (Program.NewGameForm.radioButton3.Checked)
            {
                str = $"C:/workV/mobDev2/wfaPyatnashki/img2/";
                format = $"jpg";

            }
           
                tableLayoutPanel1.Controls.Clear();

            for (int i = 0; i < n; i++)
            {
                
                for (int j = 0; j < n; j++)
                {
                    pic1[i, j] = new PictureBox();
                    pic1[i, j].Image = Image.FromFile($"{str}{matrix[i, j]}.{format}");
                    pic1[i, j].Name = $"{matrix[i, j]}";
                    pic1[i, j].Size = new System.Drawing.Size(this.ClientRectangle.Width / n,
                        this.ClientRectangle.Height / n);
                    pic1[i, j].BackColor = Color.Blue;
                    pic1[i, j].SizeMode = PictureBoxSizeMode.StretchImage;
                    tableLayoutPanel1.Controls.Add(pic1[i, j]);
                    pic1[i, j].Click += Pic2_Click;
                }
            }
        }

        private void Pic2_Click(object sender, EventArgs e)
        {
            int x=-1, y=-1;
            string name = (sender as PictureBox).Name;
            for (int j = 0; j < n; j++)
            {    for (int i = 0; i < n; i++)
                {
                   if(matrix[i, j].ToString() == name)
                    {
                        x = i;
                        y = j;
                    }
                }
            }
            if ((x != -1 && y != -1)&& (x==_x || y == _y))
            {
                Hod(x,y);
            }

          
            //   MessageBox.Show(
            //"Выберите один из вариантов",
            //"Сообщение",
            // MessageBoxButtons.YesNo,
            //MessageBoxIcon.Information,
            // MessageBoxDefaultButton.Button1,
            // MessageBoxOptions.DefaultDesktopOnly);

        }

        void FillMatrix(int n)
        {
            int k=0;
            for (int j = 0; j < n; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    if(i ==n-1 && j==n-1)
                        matrix[i, j] = 0;
                    else
                        matrix[i, j] = i * n + j + 1;


                }
            }
            _x = n-1;
            _y = n-1;
          
        }
      

        private void новаяИграToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.NewGameForm.ShowDialog();
            gameStart();
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void таймерToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Form2.Show();
        }

        private void рекордыToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }





        void Hod(int x, int y)
        {
            if (gameBegin)
            hodCount++;
            Program.Form2.hodCount.Text = hodCount.ToString();
            if (x >=n  || y >= n || x<0 || y<0)
                return;

            if (x == _x && y == _y)
                return;

            if (x == _x)
            {
                if (y > _y)
                {
                    while (y != _y)
                    {
                        matrix[_x, _y] = matrix[_x, _y + 1];
                        _y = _y + 1;
                        matrix[_x, _y] = 0;

                    }

                }
                else if (y < _y)
                {
                    while (y != _y)
                    {
                        matrix[_x, _y] = matrix[_x, _y - 1];
                         _y = _y - 1;
                         matrix[_x, _y] = 0;
                    }
                }
            }
            else if (y == _y)
            {
                if (x > _x)
                {
                    while (x != _x)
                    {
                        matrix[_x, _y] = matrix[_x + 1, _y];
                        _x = _x + 1;
                        matrix[_x, _y] = 0;
                    }
                }
                else if (x < _x)
                {
                    while (x != _x)
                    {
                        matrix[_x, _y] = matrix[_x - 1, _y];
                        _x = _x - 1;
                        matrix[_x, _y] = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show(
          "Выберите один из вариантов",
          "Сообщение",
           MessageBoxButtons.YesNo,
          MessageBoxIcon.Information,
           MessageBoxDefaultButton.Button1,
           MessageBoxOptions.DefaultDesktopOnly);
            }
            ShowMatrix(n);
            CheckResult();
        }
        void CheckResult()
        {
            if (gameBegin)
            {
                Program.Form2.richTextBox1.Clear();
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        Program.Form2.richTextBox1.AppendText(matrix[i, j].ToString() + " ");
                    }

                }


                int res = 0;
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (matrix[i, j] == i * n + j + 1)
                            res++;
                    }

                }


                if (res == n * n - 1)
                    MessageBox.Show(
            "ПОБЕДА!",
            "Поздравляем!",
             MessageBoxButtons.YesNo,
            MessageBoxIcon.Information,
             MessageBoxDefaultButton.Button1,
             MessageBoxOptions.DefaultDesktopOnly);
            }
        }
    }
}
