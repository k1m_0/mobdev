﻿using System;

namespace ConsoleRofls
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach(ConsoleColor color in ConsoleColor.GetValues(typeof(ConsoleColor)))
            {
                Console.BackgroundColor = color;
                Console.Write(new String(' ', 5));
                Console.ResetColor();
                Console.ForegroundColor = color;
                Console.WriteLine(color);
            }

            Console.WriteLine($"|{123,-12}|{123,5}|");
        }
    }
}
