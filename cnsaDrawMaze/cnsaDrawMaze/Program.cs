﻿using System.Collections.Generic;
int height = 75, width = 45;
int[,] maze = new int[height, width];
int unvisited=0;
Stack<Point> _path = new Stack<Point>();
var arrNeighbours = new List<Point>();
Random rnd = new Random();
main();
void main()
{
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            if ((i % 2 != 0 && j % 2 != 0) &&(i < height - 1 && j < width - 1))
            {  
                maze[i, j] = 1;

                unvisited++;
            }
            else maze[i, j] = 0; 
           
        }
    }
    
    
    DrawMaze();
    maze[0, 1] = 2;
    maze[height-1, width-2] = 2;
    ShowMaze();
}


void ShowMaze(int x=-1, int y=-1 )
{
    if (x == -1)
    {
       Console.SetCursorPosition(0, 0);
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (maze[i, j] == 1) Console.BackgroundColor = ConsoleColor.White;
                else if (maze[i, j] == 0) Console.BackgroundColor = ConsoleColor.Cyan;
                else Console.BackgroundColor = ConsoleColor.White;
                Console.Write("  ");
            }
            Console.WriteLine();
        }
        Console.ResetColor();
    }
    else
    {
        Point pntC;
          pntC.x=  Console.GetCursorPosition().Top;
          pntC.y=  Console.GetCursorPosition().Left;
        Console.BackgroundColor = ConsoleColor.White;
        Console.SetCursorPosition(y*2, x);
        Console.Write("  ");
        Console.SetCursorPosition( pntC.y, pntC.x);
    }
}
void DrawMaze()
{   Point currentCoord = new Point();
    Point choosed= new Point();
    currentCoord.x = 1;
    currentCoord.y = 1;
    currentCoord.x = rnd.Next(0,(height-1)/2)*2+1;
    currentCoord.y = rnd.Next(0, (width- 1) / 2) * 2 + 1;
    maze[currentCoord.x, currentCoord.y] = -1;
    unvisited--;
    //getNeighbours(currentCoord);
    while (unvisited != 0)
    {
        getNeighbours(currentCoord);
        if (arrNeighbours.Count!=0)
        {
            _path.Push(currentCoord);
            int rnd1 = rnd.Next(arrNeighbours.Count);
              choosed = arrNeighbours[rnd1];
            deleteWall(currentCoord,choosed);
            maze[choosed.x, choosed.y] = -1;
            unvisited--;
            currentCoord = choosed;
            ShowMaze(choosed.x,choosed.y);
            Point pntC;
            pntC.x = Console.GetCursorPosition().Top;
            pntC.y = Console.GetCursorPosition().Left;
            Console.ReadLine();
            Console.SetCursorPosition(pntC.y, pntC.x);
        }
        else if (_path.Count != 0)
        {
            currentCoord = _path.Pop();
        }
        else
        {
            currentCoord= chooseRandNewCell();
        }
    }
}
Point chooseRandNewCell()
{
    Point newCell = new Point();
    newCell.x = 0;
    newCell.y = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            if (maze[i, j] == 1)
            {
                newCell.x = i;
                newCell.y = j;
                return newCell;
            }
        }
    }
    return newCell;
}
void deleteWall(Point cC,Point cH)
{
    int x, y;
    if (cC.x == cH.x){
        x = cH.x;
        y = cH.y + ( (cC.y > cH.y) ? 1 : -1 );
    }else{
        y = cH.y;
        x = cH.x + ( (cC.x > cH.x) ? 1 : -1 );
    }
    maze[x, y] = -1;
    ShowMaze(x, y);
}
void getNeighbours(Point cC)
{
    Point tempP;
    arrNeighbours.Clear();
   
    tempP.x = cC.x + 2;
    tempP.y = cC.y;
    if ( (tempP.x < height) && (maze[tempP.x, tempP.y] != -1) ) {
        arrNeighbours.Add(tempP);
    }

    tempP.x = cC.x - 2;
    tempP.y = cC.y;
    if (tempP.x > 0 && maze[tempP.x, tempP.y] != -1){
        arrNeighbours.Add(tempP);
    }

    tempP.x = cC.x ;
    tempP.y = cC.y + 2;
    if(tempP.y < width && maze[tempP.x, tempP.y] != -1 ){
        arrNeighbours.Add(tempP);
    }

    tempP.x = cC.x;
    tempP.y = cC.y - 2;
    if (tempP.y > 0 && maze[tempP.x, tempP.y] != -1){
        arrNeighbours.Add(tempP);
    }

}

struct Point
{
    public int x, y;
};
