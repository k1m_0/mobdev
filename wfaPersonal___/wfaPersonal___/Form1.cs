using System.Runtime.InteropServices;
using System.Threading;

namespace wfaPersonal___
{
    public partial class Form1 : Form
    { 

        Thread[] mythread = new Thread[8];
        private Bitmap b;
        private Graphics g;
        private Random rnd = new Random();
        private int counter;
        private int _y;
        private bool GlFlag=true;
        private byte ggg;
        public bool GlobStep = false;
        private static bool _run = true;
        //ggg && 255
        static int x=80, y=150;
        static char[,] charMatr ;
        private static bool[] readyStep = new bool[8];
        private static bool[] nextStep = new bool[8];
        private static string strP = "";       
    //ggg || 8
    private static Color[,] arrColor = new Color[1000, 1000];
        private static bool nextStep0;
        private string fileName;

        public Form1()
        {
            InitializeComponent();
           

            this.Width = pictureBox1.Image.Width + 300;
            this.Height= pictureBox1.Image.Height;
            pbDraw.Width = pictureBox1.Image.Width;
            pbDraw.Height = pictureBox1.Image.Height;
                 charMatr = new char[x, y];
                b = new Bitmap(pbDraw.Width, pbDraw.Height);
                g = Graphics.FromImage(b);
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    charMatr[i, j] = AsciiCharacters;
                }
            }
            for (int i = 0; i < y; i++)
            {
                strP += charMatr[x - 1, i];
            }
            mythread[0] = new Thread(fornewthread0);
            mythread[1] = new Thread(fornewthread1);
            mythread[2] = new Thread(fornewthread2);
            mythread[3] = new Thread(fornewthread3);
            mythread[4] = new Thread(fornewthread4);
            mythread[5] = new Thread(fornewthread5);
            mythread[6] = new Thread(fornewthread6);
            mythread[7] = new Thread(fornewthread7);
            getColorArr(GlFlag);
            for (int i = 0; i < 8; i++)
            {
                mythread[i].Start();
            }
            timer1.Tick += Timer1_Tick1;
            pbDraw.Paint += (s, e) =>
            {
                e.Graphics.DrawImage(b, 0, 0);
            };
            timer1.Start();
        }
        void fornewthread0(){fornewthreadUni(0);}
        void fornewthread1(){fornewthreadUni(1);}
        void fornewthread2(){fornewthreadUni(2);}
        void fornewthread3(){fornewthreadUni(3);}
        void fornewthread4(){fornewthreadUni(4);}
        void fornewthread5(){fornewthreadUni(5);}
        void fornewthread6(){fornewthreadUni(6);}
        void fornewthread7(){fornewthreadUni(7);}

        static void fornewthreadUni(int thrN)
        {

            while (_run)
            {
                readyStep[thrN] = false;
        
                for (int i = x - 1; i > 0; i--)
                {
                    for (int k = 0; k < y/8; k++)
                    {
                        charMatr[i, thrN+k*8] = charMatr[i - 1, thrN+ k * 8];
                    }
                    }

                for (int k = 0; k < y / 8; k++)
                {
                    charMatr[0, thrN+ k * 8] = strP[thrN+ k * 8];
                }
                readyStep[thrN] = true;
                while (nextStep[thrN] && _run){}
                nextStep[thrN] = true;
            }
        }
        private void Timer1_Tick1(object? sender, EventArgs e)
        {
            g.Clear(Color.Black);

            bool isReadyAll = false;
            while (!isReadyAll && _run)
            {
                for (int i = 0; i < readyStep.Length; i++)
                {
                    if (i == 0 )
                        isReadyAll = readyStep[i];
                  
                    isReadyAll = isReadyAll && readyStep[i];
                }
            }
            strP = "";
            for (int i = 0; i <y; i++)
            {
                strP += charMatr[x-1, i];
            }

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                g.DrawString(charMatr[i, j].ToString(), new Font("Courier New", 10.0F, FontStyle.Bold), new SolidBrush(arrColor[i,j]), new Point(12*j, 12*i));
                }
            }
            for (int i = 0; i < 8; i++)
            {
                nextStep[i] = GlobStep;

            }
            pbDraw.Invalidate();
        }

        void getColorArr(bool flag)
        {
            
            Bitmap bmp = new Bitmap(pictureBox1.Image);

            int _Alp;
            int aa=0, bb = 0;
            int ty=1;
            for (int yy = 0; yy < bmp.Height; yy++)
            {
                for (int xx = 0; xx < bmp.Width; xx++)
                {

                    if (xx % 12 == 0 && yy % 12 == 0)
                    {
                        _Alp = (bmp.GetPixel(xx, yy).B + bmp.GetPixel(xx, yy).G + bmp.GetPixel(xx, yy).R) ;
                        _Alp = (_Alp > 160 && _Alp < 220) ? 220 : _Alp;
                        _Alp = (_Alp > 240) ? 240 : _Alp;
                        if (flag)
                        {
                            _Alp = (bmp.GetPixel(xx, yy).B + bmp.GetPixel(xx, yy).G + bmp.GetPixel(xx, yy).R);
                            _Alp = (_Alp > 160 && _Alp < 220) ? 220 : _Alp;
                            _Alp = (_Alp > 240) ? 240 : _Alp;
                            arrColor[aa, bb] = Color.FromArgb(_Alp, bmp.GetPixel(xx, yy).R, bmp.GetPixel(xx, yy).G, bmp.GetPixel(xx, yy).B);
                        }
                        else
                        {
                            _Alp = (bmp.GetPixel(xx, yy).B + bmp.GetPixel(xx, yy).G + bmp.GetPixel(xx, yy).R)/3;
                            _Alp = (_Alp > 160 && _Alp < 220) ? 220 : _Alp;
                            _Alp = (_Alp > 240) ? 240 : _Alp;
                            arrColor[aa, bb] = Color.FromArgb(_Alp, 0, 170, 0);
                        }
                        bb++;
                    }   
                }
                bb = 0;
                if (yy % 12 == 0)
                {
                    aa++;
                }
                }
        }
        char AsciiCharacters
        {
            get
            {
                int t = rnd.Next(90);
                string a = char.ConvertFromUtf32(12449+t);
                return Convert.ToChar(a);
            }
        }
    
       
      
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _run = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            GlFlag = !GlFlag;
            getColorArr(GlFlag);
        }

        private void btLoadNew_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
               
            }
            
            getColorArr(GlFlag);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            GlobStep = !GlobStep;
            if (GlobStep)
                timer1.Stop();
            else
                timer1.Start();
        }

       
    }
}
//GlobStep = !GlobStep;

