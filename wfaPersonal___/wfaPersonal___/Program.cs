namespace wfaPersonal___
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        // public static Form1 Form1;
        [STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();
           // Program.Form1 = new Form1();
            Application.Run(new Form1());
        }
    }
}