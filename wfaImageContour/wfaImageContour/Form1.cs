namespace wfaImageContour
{
    public partial class Form1 : Form
    {
        Bitmap bmp;
        private Point startPoint;
        private Point maxL;
        private Point minR;
        private Point startCircle;
        private Point curPoint = new Point(0,0);
       
        enum Direction
        {
            up,
            down,
            left,
            right
        }
        private Direction _dir = Direction.right;
        private bool ws = true;
        private int[,] matrix;



        public Form1()
        {
            InitializeComponent();
            this.MouseDown += Form1_MouseDown;
            pictureBox1.MouseDown += PictureBox1_MouseDown;
            pictureBox1.MouseClick += PictureBox1_MouseClick;
            
        }

        private void PictureBox1_MouseClick(object? sender, MouseEventArgs e)
        {     
            pictureBox1.Invalidate(false);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bmp = new Bitmap(pictureBox1.Image);
            pictureBox1.BackColor = Color.White;
            matrix = new int[pictureBox1.Image.Height + 1, pictureBox1.Image.Width + 1];
            
        }

       

        private void PictureBox1_MouseDown(object? sender, MouseEventArgs e)
        {
            
            

            startPoint = e.Location;
            Graphics g = pictureBox1.CreateGraphics();
            using (var bmp = new Bitmap(pictureBox1.Image.Width, pictureBox1.Image.Height))
            {
               
                pictureBox1.DrawToBitmap(bmp, pictureBox1.ClientRectangle);
                if (bmp.GetPixel(startPoint.X, startPoint.Y) != Color.FromArgb(255,255,255,255))
                {
                    curPoint = startPoint;
                    while (bmp.GetPixel(curPoint.X, curPoint.Y) != Color.FromArgb(255, 255, 255, 255))
                    {
                        curPoint.Y -= 1;
                    }
                    startCircle = curPoint;

                    do
                    {
                        ws = false;
                        // ������
                        if (
                             bmp.GetPixel(curPoint.X + 1, curPoint.Y) == Color.FromArgb(255, 255, 255, 255)&&
                           
                             (
                                bmp.GetPixel(curPoint.X, curPoint.Y + 1) != Color.FromArgb(255, 255, 255, 255) 
                                ||
                                bmp.GetPixel(curPoint.X + 1, curPoint.Y+1) != Color.FromArgb(255, 255, 255, 255)
                             )
                           )
                        {
                            curPoint.X += 1;
                            g.DrawRectangle(Pens.Red, curPoint.X, curPoint.Y,1,1);
                        }
                        //����
                        if (
                              bmp.GetPixel(curPoint.X, curPoint.Y+1) == Color.FromArgb(255, 255, 255, 255) &&
                              (
                                 bmp.GetPixel(curPoint.X-1, curPoint.Y) != Color.FromArgb(255, 255, 255, 255)
                                 ||
                                 bmp.GetPixel(curPoint.X - 1, curPoint.Y + 1) != Color.FromArgb(255, 255, 255, 255)
                              )
                            )
                        {
                            curPoint.Y += 1;
                            g.DrawRectangle(Pens.Red, curPoint.X, curPoint.Y, 1, 1);
                        }
                        //�����
                        if (
                        bmp.GetPixel(curPoint.X - 1, curPoint.Y) == Color.FromArgb(255, 255, 255, 255) &&
                        (
                           bmp.GetPixel(curPoint.X, curPoint.Y-1) != Color.FromArgb(255, 255, 255, 255)
                           ||
                           bmp.GetPixel(curPoint.X - 1, curPoint.Y - 1) != Color.FromArgb(255, 255, 255, 255)
                        )
                      )
                        {
                            curPoint.X -= 1;
                            g.DrawRectangle(Pens.Red, curPoint.X, curPoint.Y, 1, 1);
                        }

                        //�����
                        if (
                      bmp.GetPixel(curPoint.X, curPoint.Y-1) == Color.FromArgb(255, 255, 255, 255) &&
                      (
                         bmp.GetPixel(curPoint.X+1, curPoint.Y) != Color.FromArgb(255, 255, 255, 255)
                         ||
                         bmp.GetPixel(curPoint.X + 1, curPoint.Y - 1) != Color.FromArgb(255, 255, 255, 255)
                      )
                    )
                        {
                            curPoint.Y -= 1;
                            g.DrawRectangle(Pens.Red, curPoint.X, curPoint.Y, 1, 1);
                        }



                    } while (curPoint != startCircle);
                    this.Text = "complete";

                }
            }


        }

        private void Form1_MouseDown(object? sender, MouseEventArgs e)
        {
         
        }

       
    }
}