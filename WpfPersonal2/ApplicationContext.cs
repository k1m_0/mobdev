﻿using System;
using Microsoft.EntityFrameworkCore;
namespace WpfPersonal2.Data
{
    public class ApplicationContext : DbContext
    {

        //public DbSet<USER> USERS { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<USER> USERS { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<USER>().HasData(GetUsers());
            base.OnModelCreating(modelBuilder);
        }
        private USER[] GetUsers()
        {
            return new USER[]
            {
                new USER { ID = 1, LOGIN = "admin", PASSWORD = "admin", REMAIL = "admin@admin.admin"},
                new USER { ID = 2, LOGIN = "user1", PASSWORD = "admin", REMAIL = "admin@admin.admin"},
                new USER { ID = 3, LOGIN = "user2", PASSWORD = "admin", REMAIL = "admin@admin.admin"},
                new USER { ID = 4, LOGIN = "user3", PASSWORD = "admin", REMAIL = "admin@admin.admin"},
            };
        }
    }
}
