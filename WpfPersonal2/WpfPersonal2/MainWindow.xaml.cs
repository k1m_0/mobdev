﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using SQLite;
using WpfPersonal2.Game;

namespace WpfPersonal2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool IsDarkTheme { get; set; }
        private readonly PaletteHelper paletteHelper = new PaletteHelper();
        public bool isBlured;




        public USERS user = new USERS();
        public SQLiteConnection Db;
        public Array UserArr;
        public string userLogged;



        public MainWindow()
        {
            InitializeComponent();
            InitializeDb();
            ShowAdPanel();
           
        }
        private void InitializeDb()
        {
            Db = new SQLiteConnection("USERS.db");
            Db.CreateTable<USERS>();
            UserArr = Db.Table<USERS>().ToArray();
        }
        [Table("USERS")]
        public class USERS
        {
            [PrimaryKey]
            public string LOGIN { get; set; }
            public string PASSWORD { get; set; }
            public string REMAIL { get; set; }
            public int HIGH_SCORE { get; set; }


        }


        private void RegTextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtUsername.Text != "" && txtPassword.Password.ToString() != "")
                loginBtn.IsEnabled = true;

        }
        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtUsername.Text != "" && txtPassword.Password.ToString() != "")
                loginBtn.IsEnabled = true;
        }

        private void themeToggle_Click(object sender, RoutedEventArgs e)
        {
       
            ITheme theme = paletteHelper.GetTheme();

            if (IsDarkTheme = theme.GetBaseTheme() == BaseTheme.Dark)
            {
                IsDarkTheme = false;
                theme.SetBaseTheme(Theme.Light);
            }
            else
            {
                IsDarkTheme = true;
                theme.SetBaseTheme(Theme.Dark);
            }
            paletteHelper.SetTheme(theme);
        }

        private void Btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

      

        private void loginBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach(USERS i in UserArr)
            {
                if(i.LOGIN == txtUsername.Text && i.PASSWORD == txtPassword.Password.ToString())
                {
                    this.Hide();
                    ArcanoidGame gameWindow = new();
                    userLogged = txtUsername.Text;
                    gameWindow.ShowDialog();

                }
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
           
            base.OnMouseLeftButtonDown(e);
            DragMove();
    
        }

        private void signupBtn_Click(object sender, RoutedEventArgs e)
        {
            isBlured = true;
            BlureMyStack(isBlured);
            RegWindow regWindow = new();
            regWindow.ShowDialog();
        }
        private void ShowAdPanel()
        {  
           AdminPanel admPanel = new();
            admPanel.Show();
        }

        public void BlureMyStack(bool flag)
        {
            if (mainStack == null)
            {
                return;
            }
            if(flag)
            mainStack.Effect = new BlurEffect
            {
                KernelType = KernelType.Gaussian,
                Radius = 10
            };
            else
                mainStack.Effect = null;


        }
    }
}
