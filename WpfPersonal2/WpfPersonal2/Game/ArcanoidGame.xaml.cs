﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static WpfPersonal2.MainWindow;

namespace WpfPersonal2.Game
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    
    public partial class ArcanoidGame : Window
    {
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        private double plVelocity=10;
        Ball myBall;
        Platform myPlatform;
        Block[] blocks;
        public static int[,] gameMap ;
        public static int[] importMap;

        public ArcanoidGame()
        {
            InitializeComponent();
            //Canvas.Bottom = "10" Canvas.Left = "540"
            myBall = new Ball((int)canvGame.Width / 2, 25);
            this.Title = $"Осталось жизней {myBall.Hp.ToString()}";
            gameMap = new int[(int)canvGame.Width, (int)canvGame.Height];
            for (int w = 0; w < (int)canvGame.Width; w++)
            {
                for (int h = 0; h < (int)canvGame.Height; h++)
                {
                    gameMap[w, h] = -1;
                }
            }
            //Canvas.Bottom = "10" Canvas.Left = "540"

            myPlatform = new Platform((int)canvGame.Width/2-100, 10);
            Canvas.SetLeft(ball, myBall.bp.X);
            Canvas.SetBottom(ball, myBall.bp.Y); 
            Canvas.SetLeft(plPlatform, myPlatform.bp.X);
            Canvas.SetBottom(plPlatform, myPlatform.bp.Y);
            PlaceBlocks(true);
            test();
            Canvas.SetZIndex(ball, 100);
        }

        private void PlaceBlocks(bool flag)
        {
            //Rectangle q = new Rectangle();
            //q.Width = (int)canvGame.Width;
            //q.Height = (int)canvGame.Height;
            //q.Fill = Brushes.Red;

            //canvGame.Children.Add(q);
            if (flag)
            {
                blocks = new Block[10];
                for (int i = 0; i < blocks.Length; i++)
                {
                   
                    blocks[i] = new Block(i, 100);
                    Rectangle brick = new Rectangle();
                    brick.Width = 60;
                    brick.Height = 15;
                    for (int h = 500; h < 515; h++)
                    {
                        for (int w = (10 + i * 80); w < i * 80 + 60; w++)
                        {
                            gameMap[w, h] = i;
                        }
                    }
                    brick.Fill = Brushes.Green;
                    brick.Name = $"block{i}";
                    canvGame.Children.Add(brick);
                    Canvas.SetLeft(brick, 10 + i * 80);
                    Canvas.SetTop(brick, 70);
                }
            }
            else
            {
                
            }
            
        }

        private void test()
        {
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(20);
            dispatcherTimer.Start();
           
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            bCoords.Text = myBall.bp.ToString();
            if (!myBall.isOnPlatform)
            {
                IsCollision();
            }
        }

        private void IsCollision()
        {
            myBall.bp.X = myBall.bp.X + myBall.bVelX;
            myBall.bp.Y = myBall.bp.Y + myBall.bVelY;
            Canvas.SetLeft(ball, myBall.bp.X);
            Canvas.SetBottom(ball, myBall.bp.Y);

            
             if (myBall.bp.X + 20 >= canvGame.Width)
            {
                myBall.bVelX = -myBall.bVelX;
            }
            else if (myBall.bp.X <= 0)
            {
                myBall.bVelX = -myBall.bVelX;
            }
            else if(myBall.bp.Y + 20 >= canvGame.Height)
            {
                myBall.bVelY = -myBall.bVelY;
            }
            else if(myBall.bp.Y - 20 <= 0)
            {
                if (Canvas.GetLeft(plPlatform) < myBall.bp.X && Canvas.GetLeft(plPlatform) + 200 > myBall.bp.X)
                {
                    myBall.bVelY = -myBall.bVelY;
                }
                else
                {
                    myBall.isOnPlatform = true;
                    myBall.bp.X = myPlatform.bp.X + 100;
                    myBall.bp.Y = myPlatform.bp.Y + 15;
                    Canvas.SetLeft(ball, myBall.bp.X);
                    Canvas.SetBottom(ball, myBall.bp.Y);
                    myBall.Hp--;
                    this.Title = $"Осталось жизней {myBall.Hp.ToString()}";
                }
            }
            else if (gameMap[(int)myBall.bp.X, (int)myBall.bp.Y + 2] != -1)
            {

                DamageWall(gameMap[(int)myBall.bp.X, (int)myBall.bp.Y + 2]);
                myBall.bVelY = -myBall.bVelY;
            }
            else if (gameMap[(int)myBall.bp.X -2 , (int)myBall.bp.Y] != -1)
            {
                DamageWall(gameMap[(int)myBall.bp.X - 2, (int)myBall.bp.Y]);
                myBall.bVelX = -myBall.bVelX;
            }
            else if (gameMap[(int)myBall.bp.X, (int)myBall.bp.Y - 20] != -1)
            {
                DamageWall(gameMap[(int)myBall.bp.X, (int)myBall.bp.Y - 20]);
                myBall.bVelY = -myBall.bVelY;
            }
            else if (gameMap[(int)myBall.bp.X +20, (int)myBall.bp.Y] != -1)
            {
                DamageWall(gameMap[(int)myBall.bp.X + 20, (int)myBall.bp.Y]);
                myBall.bVelX = -myBall.bVelX;
            }
        }

        private void DamageWall(int v)
        {
            counter+=200;
            bMC.Text = $"Ваш счёт{counter}";
            blocks[v].Delete();
            Rectangle q = new Rectangle();


            q.Width = 60;
            q.Height = 20;
            
            q.Fill = Brushes.White;
            
            canvGame.Children.Add(q);
            Canvas.SetLeft(q, 10 + v * 80);
            Canvas.SetTop(q, 67);

            for (int w = 0; w < (int)canvGame.Width; w++)
            {
                for (int h = 0; h < (int)canvGame.Height; h++)
                {
                    if (gameMap[w, h] == v)
                    {
                        gameMap[w, h] = -1;
                        
                       


                    }

                }
            }
            if (counter == 2000)
            {
                var mainW = ((MainWindow)Application.Current.MainWindow);

                mainW.Db.Update(new USERS() { LOGIN = mainW.userLogged,PASSWORD = "admin", REMAIL = "admin@admin.admin", HIGH_SCORE = 1000 });
                myBall.isOnPlatform = true;
                myBall.bp.X = myPlatform.bp.X + 100;
                myBall.bp.Y = myPlatform.bp.Y + 15;
                Canvas.SetLeft(ball, myBall.bp.X);
                Canvas.SetBottom(ball, myBall.bp.Y);
                Window1 window1 = new();
                window1.Show();
                BlureMyStack(true);
            }
        }
        public void BlureMyStack(bool flag)
        {
            if (mainStack == null)
            {
                return;
            }
            if (flag)
                mainStack.Effect = new BlurEffect
                {
                    
                    KernelType = KernelType.Gaussian,
                    Radius = 10
                };
            else
                mainStack.Effect = null;


        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                  //case Key.LeftShift:
                  //  if(e.Key == Key.Left)
                  //  {
                  //      Canvas.SetLeft(plPlatform, Canvas.GetLeft(plPlatform) - plVelocity*10);
                  //  }
                  //  if (e.Key == Key.Right)
                  //  {
                  //      Canvas.SetLeft(plPlatform, Canvas.GetLeft(plPlatform) + plVelocity*10);
                  //  }
                  //  break;

                case Key.Left:
                    if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
                    {
                        myPlatform.bp.X -= plVelocity * 10;
                        Canvas.SetLeft(plPlatform, myPlatform.bp.X);
                        if (myBall.isOnPlatform)
                        {
                            Canvas.SetLeft(ball, myPlatform.bp.X+100);
                            myBall.bp.X = myPlatform.bp.X + 100;
                        }
                    }
                    else
                    {
                        myPlatform.bp.X -= plVelocity;
                        Canvas.SetLeft(plPlatform, myPlatform.bp.X);
                        if (myBall.isOnPlatform)
                        {
                            Canvas.SetLeft(ball, myPlatform.bp.X + 100);
                            myBall.bp.X = myPlatform.bp.X + 100;
                        }
                    }
                            break;
                case Key.Right:
                    if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
                    {
                        myPlatform.bp.X += plVelocity * 10;
                        Canvas.SetLeft(plPlatform, myPlatform.bp.X);
                        if (myBall.isOnPlatform)
                        {

                            Canvas.SetLeft(ball, myPlatform.bp.X + 100);
                            myBall.bp.X = myPlatform.bp.X + 100;
                        }

                    }
                    else
                    {
                        myPlatform.bp.X += plVelocity;
                        Canvas.SetLeft(plPlatform, myPlatform.bp.X);
                        if (myBall.isOnPlatform)
                        {
                            Canvas.SetLeft(ball, myPlatform.bp.X + 100);
                            myBall.bp.X = myPlatform.bp.X + 100;
                        }
                    }
                    break;

              
                case Key.Space:
                    if (myBall.isOnPlatform)
                    {
                        myBall.isOnPlatform = false;
                    }
                    break;

               
                case Key.RightShift:
                    break;
                case Key.LeftCtrl:
                    break;
                case Key.RightCtrl:
                    break;
                case Key.LeftAlt:
                    break;
                case Key.RightAlt:
                    break;
             
            }
        }

        private void canvGame_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //int _x = (int)e.GetPosition(null).X - 233;
            //int _y = (int)e.GetPosition(null).Y - 162;
            //bMC.Text = "X=" + _x + " Y=" + _y;
            //bArrVal.Text = gameMap[_x,600-_y].ToString();
        }

        private void opfBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                string s;
                s = System.IO.File.ReadAllText(openFileDialog.FileName).Replace("\n", "+").Replace(" ","+");
                string[] str1 = s.Split('+');
                for (int w = 0; w < (int)canvGame.Width; w++)
                {
                    for (int h = 0; h < (int)canvGame.Height; h++)
                    {
                        gameMap[w, h] = Convert.ToInt32(str1[h+w]);
                    }
                }
                PlaceBlocks(false);
            }
        }
        public int counter=0;
    }
}



public class Block
{
    public Point bp;   
    public int Hp;
    public Block(int x,int y)
    {
        bp.X = x;
        bp.Y = y;
        Hp = 1;

    }
    public void Delete()
    {
        Hp = 0;
    }

}
public class Ball
{
    public Ball(int x,int y)
    {
        bp.X = x;
        bp.Y = y;
        isOnPlatform = true;
        bVelX = 5;
        bVelY = 5;
        Hp = 5;
        
    }
    public int Hp;
    public bool isOnPlatform ;
    public Point bp;
    public int bVelX, bVelY;

}
public class Platform
{
    public Platform(int x, int y)
    {
        bp.X = x;
        bp.Y = y;
    }
    public Point bp;
    public int plVelocity = 20;

}

