﻿using System;
namespace WpfPersonal2.Data
{
	public class USER
	{
		public int ID { get; set; }
		public string LOGIN { get; set; }
		public string PASSWORD { get; set; }
		public string REMAIL { get; set; }

		public USER()
		{
		}

		public USER(string LOGIN, string PASSWORD, string REMAIL)
		{
			this.LOGIN = LOGIN;
			this.PASSWORD = PASSWORD;
			this.REMAIL = REMAIL;
		}
	}
}