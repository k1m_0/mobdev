﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static WpfPersonal2.MainWindow;
namespace WpfPersonal2
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class RegWindow : Window
    {
        public string Login { get; private set; }
        public string pass { get; private set; }
        public string rEmail { get; private set; }

        public RegWindow()
        {
            InitializeComponent();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();

        }

     

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            var mainW = ((MainWindow)Application.Current.MainWindow);
            mainW.isBlured = false;
            mainW.BlureMyStack(mainW.isBlured);
            this.Close();
        }

        private void regBtn_Click(object sender, RoutedEventArgs e)
        {
            var mainW = ((MainWindow)Application.Current.MainWindow);

           
           // mainW.Db.Insert(new USERS() { LOGIN = txtUsername.Text, PASSWORD = txtPassword.Password.ToString(), REMAIL = txtREmail.Text, HIGH_SCORE = 20 });
            mainW.Db.Insert(new USERS() { LOGIN = Login, PASSWORD = pass, REMAIL = rEmail, HIGH_SCORE = 20 });


            
            mainW.isBlured = false;
            mainW.BlureMyStack(mainW.isBlured);
            this.Close();
        }

        private void RegTextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtUsername.Text != "" && txtPassword.Password.ToString() != "")
                regBtn.IsEnabled = true;

        }
        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtUsername.Text != "" && txtPassword.Password.ToString() != "")
                regBtn.IsEnabled = true;
        }
    }
}
