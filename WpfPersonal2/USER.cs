﻿using System;
namespace WpfPersonal2.Data
{
	public class USER
	{
		private int ID { get; set; }
		private string LOGIN { get; set; }
		private string PASSWORD { get; set; }
		private string REMAIL { get; set; }

		public USER()
		{
		}

		public USER(string LOGIN, string PASSWORD, string REMAIL)
		{
			this.LOGIN = LOGIN;
			this.PASSWORD = PASSWORD;
			this.REMAIL = REMAIL;
		}
	}
}