﻿using System;

namespace consDrawLine
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isContinue = true;
            do
            {
                int.TryParse(Console.ReadLine(), out int w);
                Console.WriteLine(new String('*', w));


                Console.Write("Повторить? [Y/N] ->");
                isContinue = (Console.ReadLine().ToUpper() == "Y");
            }
            while (isContinue);
        }
    }
}
