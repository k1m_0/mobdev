﻿using System;

namespace NumberParse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("number?");
            int x1 = int.Parse(Console.ReadLine());
            Console.WriteLine(x1);

            int x2;
            int.TryParse(Console.ReadLine(), out x2);
            Console.WriteLine(x2);
            int x3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(x3);
        }
    }
}
