﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaToDoList
{
    public partial class addNote : Form
    {
        public addNote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.Form1.Db.Insert(new NotesData() { Name = textBox1.Text, Text = richTextBox1.Text, Date = DateTime.Now.ToString()});
            Program.Form1.updatePanelData();
            this.textBox1.Clear();
            this.richTextBox1.Clear();
        }
        
    }
}
