namespace WfaToDoList
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        public static addNote addNote;
        public static Form1 Form1;

        [STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();
            Program.Form1 = new Form1();
            Program.addNote = new addNote();
            Application.Run(Form1);
        }
    }
}