
using SQLite;
namespace WfaToDoList
{
    public partial class Form1 : Form
    {


        public NotesData noteT = new NotesData();
        public SQLiteConnection Db;
        public Array Notes;
        public string userLogged;

        public Form1()
        {
            InitializeComponent();
            addScroll();
            InitializeDb();
            updatePanelData();

        }

        public void updatePanelData()
        {
            int i=0;
            int j=0;
            getData();
            for (int n = 0; n < panel1.Controls.Count; n++)
                panel1.Controls[n].Dispose();

            foreach (NotesData n in Notes)
            {
                
                Panel myNewpanel = new Panel();
                myNewpanel.Location=new Point(150 * i,150 *j);
                myNewpanel.Height = 140;
                myNewpanel.Width= 140;
                myNewpanel.BackColor = Color.Yellow;
                panel1.Controls.Add(myNewpanel);
                Label myNewTextBox = new Label();
                myNewTextBox.Text = n.Name;

                myNewTextBox.Location = new Point(10, 0);
                myNewpanel.Controls.Add(myNewTextBox);

                RichTextBox myNewRichTextBox = new RichTextBox();
                myNewRichTextBox.Text = n.Text;
                myNewRichTextBox.Location = new Point(10, 20);
                myNewRichTextBox.Height = 80;
                myNewRichTextBox.ReadOnly=true;
                myNewRichTextBox.BackColor=Color.Yellow;


                myNewpanel.Controls.Add(myNewRichTextBox);
                

                Label myNewlabel = new Label();
                myNewlabel.Text = n.Date;

                myNewlabel.Location = new Point(10, 100);
                myNewpanel.Controls.Add(myNewlabel);
                i++;
               
                if (i>3)
                {
                    j++;
                    i = 0;
                }
            }
        }

        private void addScroll()
        {
            ScrollBar vScrollBar1 = new VScrollBar();
            vScrollBar1.Dock = DockStyle.Right;
            vScrollBar1.Scroll += (sender, e) => { panel1.VerticalScroll.Value = vScrollBar1.Value; };
            panel1.Controls.Add(vScrollBar1);
        }

        private void InitializeDb()
        {
            Db = new SQLiteConnection("USERS.db");
            Db.CreateTable<NotesData>();
          

        }

        private void getData()
        {
            Notes = Db.Table<NotesData>().ToArray();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.addNote.ShowDialog();
        }
    }
    public class NotesData
    {
        [PrimaryKey]
        public string Name { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }



    }
}