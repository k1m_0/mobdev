﻿using System;
using System.Threading;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce yourself a traveler");
            string sName;
            sName = Console.ReadLine();
            Console.WriteLine("Hello Mr. "+sName );
            Thread.Sleep(700);
            Console.WriteLine(".");
            Thread.Sleep(700);
            Console.WriteLine("..");
            Thread.Sleep(700);
            Console.WriteLine("...");
            Thread.Sleep(700);
            Console.WriteLine("Where are you from?");

            string sTown = Console.ReadLine();

            Console.WriteLine("Oh..."+ sTown +", "+ sTown + "i remember that...");
            Thread.Sleep(700);
            Console.WriteLine("So you are {0} from the {1}",sName, sTown);
            Console.WriteLine($"So you are {sName} from the {sTown}");

        }
    }
}
