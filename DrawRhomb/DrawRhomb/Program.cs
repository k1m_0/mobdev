﻿using System;

namespace DrawRhomb
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите длину полуоси");
           string length = Console.ReadLine();
            int lRh;
            int n = 1;
            int spc = 1;
            if (int.TryParse(length, out lRh))
            {
                spc = lRh+1;
                Console.WriteLine("Заливка?(Y/N)");

                if (Console.ReadLine().ToUpper() == "Y")
                {

                    for (int i = 0; i <lRh*2+1; i++)
                    {
                        
                        if (i < lRh)
                        {
                            spc--;
                            Console.Write(new String(' ', spc));
                            Console.WriteLine(new String('#',n));
                            n+= 2;
                        }
                        if (i > lRh)
                        {
                          
                            n -= 2;
                            Console.Write(new String(' ', spc));
                            Console.WriteLine(new String('#', n));
                            spc++;
                          
                        }
                    }
                }
            }


        }
    }
}
