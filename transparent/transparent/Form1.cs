namespace transparent
{
    public partial class Form1 : Form
    {

        private Bitmap b;
        private Graphics g;
        private string str;

        public Form1()
        {
            InitializeComponent();

            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g = Graphics.FromImage(b);

            pictureBox1.Paint += PictureBox1_Paint;

        }

        private void PictureBox1_Paint(object? sender, PaintEventArgs e)
        {
          
                e.Graphics.DrawImage(b, 0, 0);
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            str = "asfasdas";
            g.Clear(Color.Transparent);
            g.DrawString(str, new Font("Courier New", 20.0F), new SolidBrush(Color.Gray), new Point(30, 30));
            pictureBox1.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            str = "asfasdas";
            g.Clear(Color.Black);
            g.DrawString(str, new Font("Courier New", 20.0F), new SolidBrush(Color.Gray), new Point(30, 30));
            pictureBox1.Invalidate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            str = "asfasdas";
            g.Clear(Color.Black);
            g.DrawString(str, new Font("Courier New", 20.0F), new SolidBrush(Color.Transparent), new Point(30, 30));
            pictureBox1.Invalidate();
        }
    }
}