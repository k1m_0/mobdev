﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Random rnd = new();
        public MainWindow()
        {
            InitializeComponent();
            this.MouseDown += MainWindow_MouseDown;
        }

        private void MainWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var location = e.GetPosition(this);
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Label x = new();
                    x.SetValue(Canvas.LeftProperty, location.X);
                    x.SetValue(Canvas.TopProperty, location.Y);
                    x.Content = $"{x.GetValue(Canvas.LeftProperty):0}:{x.GetValue(Canvas.TopProperty):0}";
                    x.Background = Brushes.LightCoral;
                    this.main.Children.Add(x);
                    break;
                case MouseButton.Middle:
                    this.main.Children.Clear();
                    break;
                case MouseButton.Right:
                    for (int i = 0; i < 13; i++)
                    {
                        Label la = new();
                        la.SetValue(Canvas.LeftProperty, (double)rnd.Next((int)this.Width));
                        la.SetValue(Canvas.TopProperty, (double)rnd.Next((int)this.Height));
                        la.Content = $"{la.GetValue(Canvas.LeftProperty):0}:{la.GetValue(Canvas.TopProperty):0}";
                        la.Background = new SolidColorBrush(Color.FromArgb((byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256)));
                        this.main.Children.Add(la);
                    }
                    break;
             
            }
        }
    }
}
