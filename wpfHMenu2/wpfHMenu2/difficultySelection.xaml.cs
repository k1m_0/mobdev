﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;

namespace wpfHMenu2
{
    /// <summary>
    /// Логика взаимодействия для difficultySelection.xaml
    /// </summary>
    public partial class difficultySelection : Page
    {

        public bool IsDarkTheme { get; set; }
        private readonly PaletteHelper paletteHelper = new PaletteHelper();
        public bool isBlured;
        public difficultySelection()
        {
            InitializeComponent();
        }
        private void Btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void themeToggle_Click(object sender, RoutedEventArgs e)
        {

            ITheme theme = paletteHelper.GetTheme();

            if (IsDarkTheme = theme.GetBaseTheme() == BaseTheme.Dark)
            {
                IsDarkTheme = false;
                theme.SetBaseTheme(Theme.Light);
            }
            else
            {
                IsDarkTheme = true;
                theme.SetBaseTheme(Theme.Dark);
            }
            paletteHelper.SetTheme(theme);
        }

        private void easeDif_Click(object sender, RoutedEventArgs e)
        {

        }

        private void noramlDif_Click(object sender, RoutedEventArgs e)
        {

        }

        private void hardDif_Click(object sender, RoutedEventArgs e)
        {

        }

        private void XHDif_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
