using System.Drawing.Drawing2D;
using System.Windows.Forms;
namespace wfaPaint
{
    public partial class fmWfaPaint : Form
    {
        private Point endPoint;
        private string pntMode;
        
        public Point startPoint { get; private set; }
        private Bitmap bb;
        private Bitmap b;
        private Graphics g;
        private Pen myPen;
        private SolidBrush myBrush;
        private Point tempPoint;
        private Color clr;
        private int _size;
        Pen myArrPen;
        public fmWfaPaint()
        {
            
            InitializeComponent();
            // b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            clr = Color.Black;
            btColor.BackColor = clr;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);


            _size = Convert.ToInt32(tbSize.Text);
            //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            myPen = new Pen(clr, _size);
            myArrPen = new Pen(clr, _size);
            myBrush = new SolidBrush(clr);

           // g.DrawEllipse(myPen, 1, 1, 20, 20);

            pxArea.MouseDown += PxArea_MouseDown;
            pxArea.MouseMove += PxArea_MouseMove;
            pxArea.MouseUp += PxArea_MouseUp;
            // btPen.Click += btTool_Click;
            toolStrip1.ItemClicked += ToolStrip1_ItemClicked;

            g.Clear(Color.White);

            pxArea.Paint += (s, e) =>
            {
                e.Graphics.DrawImage(b, 0, 0);
            };
        }

        private void PxArea_MouseUp(object? sender, MouseEventArgs e)
        {
            endPoint = e.Location;
        }

        //private void DrawReversibleRectangle(int x, int y)
        //{
        //    // Hide the previous rectangle by calling the methods with the same parameters.
        //    var rect = GetSelectionRectangle(this.PointToScreen(this.reversibleRectStartPoint), this.PointToScreen(this.reversibleRectEndPoint));
        //    ControlPaint.FillReversibleRectangle(rect, Color.Black);
        //    ControlPaint.DrawReversibleFrame(rect, Color.Black, FrameStyle.Dashed);
        //    this.reversibleRectEndPoint = new Point(x, y);
        //    // Draw the new rectangle by calling
        //    rect = GetSelectionRectangle(this.PointToScreen(this.reversibleRectStartPoint), this.PointToScreen(this.reversibleRectEndPoint));
        //    ControlPaint.FillReversibleRectangle(rect, Color.Black);
        //    ControlPaint.DrawReversibleFrame(rect, Color.Black, FrameStyle.Dashed);
        //}
        private void ToolStrip1_ItemClicked(object? sender, ToolStripItemClickedEventArgs e)
        {

                    foreach(object item in toolStrip1.Items)
                    {
                    if (item is ToolStripButton)
                    {
                        if (item == e.ClickedItem)
                        {
                            if (item == btPen)
                                pntMode = "Pen";
                            if (item == btRect)
                                pntMode = "Rect";
                            if (item == btElips)
                                pntMode = "Elips";
                            if (item == btRevRect)
                                pntMode = "RevRect";
                        if (item == btPipetka)
                                pntMode = "Pipetka"; 
                        if (item == btLine)
                                pntMode = "Line"; 
                        if (item == btArrow)
                                pntMode = "Arrow";

                        }

                        if ((item != null) && (item != e.ClickedItem))
                        {
                            (item as ToolStripButton).Checked = false;
                        }
                    }
                }
            
        }

        private void btTool_Click(object? sender, EventArgs e)
        {
          
        }
        private void RestoreBtimap()
        {
            g.Dispose();
            b.Dispose();
            b = (Bitmap)bb.Clone();
            g = Graphics.FromImage(b);
        }  
        private void RestoreBtimap1()
        {
            g.Dispose();
            b.Dispose();
            b = (Bitmap)bb.Clone();
            g = Graphics.FromImage(b);
        }
        private void PxArea_MouseMove(object? sender, MouseEventArgs e)
        {

            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (pntMode == "Pen")
                    {
                        g.DrawLine(myPen, startPoint, e.Location);
                        startPoint = e.Location;
                      
                    }
                    else if (pntMode == "Rect")
                    {
                        RestoreBtimap();
                        
                        PointF point1 = new PointF(Convert.ToInt32(startPoint.X), Convert.ToInt32(startPoint.Y));
                        PointF point2 = new PointF(Convert.ToInt32(e.Location.X), Convert.ToInt32(startPoint.Y));
                        PointF point3 = new PointF(Convert.ToInt32(e.Location.X), Convert.ToInt32(e.Location.Y));
                        PointF point4 = new PointF(Convert.ToInt32(startPoint.X), Convert.ToInt32(e.Location.Y));
                        PointF[] points = new[] { point1, point2, point3, point4 };
                        if (isFilled.Checked)
                            g.FillPolygon(myBrush, points);
                        else
                            g.DrawPolygon(myPen, points);

                    }
                    else if(pntMode == "Elips")
                    {
                        RestoreBtimap();
                    
                        Point pntMax = new Point(Math.Max(startPoint.X, e.Location.X), Math.Max(startPoint.Y, e.Location.Y));
                        Point pntMin = new Point(Math.Min(startPoint.X, e.Location.X), Math.Min(startPoint.Y, e.Location.Y));
                        if (isFilled.Checked)
                            g.FillEllipse(myBrush, pntMin.X, pntMin.Y, pntMax.X - pntMin.X, pntMax.Y - pntMin.Y);
                        else
                            g.DrawEllipse(myPen, pntMin.X, pntMin.Y, pntMax.X - pntMin.X, pntMax.Y - pntMin.Y);
                    }
                    else if (pntMode == "Line")
                    {
                        RestoreBtimap();
                        g.DrawLine(myPen, startPoint, e.Location);
                        

                    }
                    else if(pntMode == "Arrow")
                    {
                        RestoreBtimap();

                        myArrPen.Width=_size;
                        myArrPen.CustomEndCap = new AdjustableArrowCap(3, 5);
                        g.DrawLine(myArrPen, startPoint, e.Location);
                    }
                 
                    else if (pntMode == "RevRect")
                    {
                        //  RestoreBtimap1();
                        //Point pntMax1 = new Point(Math.Max(startPoint.X, tempPoint.X), Math.Max(startPoint.Y, tempPoint.Y));
                        //Point pntMin1 = new Point(Math.Min(startPoint.X, tempPoint.X), Math.Min(startPoint.Y, tempPoint.Y));
                        //var rect1 = new Rectangle(pntMin1.X, pntMin1.Y, pntMax1.X - pntMin1.X, pntMax1.Y - pntMin1.Y);
                        //ControlPaint.DrawReversibleFrame(rect1, Color.Cyan, FrameStyle.Thick);
                        
                        Point pntMax = new Point(Math.Max(startPoint.X, e.Location.X), Math.Max(startPoint.Y, e.Location.Y));
                        Point pntMin = new Point(Math.Min(startPoint.X, e.Location.X), Math.Min(startPoint.Y, e.Location.Y));
                        var rect = new Rectangle(pntMin.X, pntMin.Y, pntMax.X - pntMin.X, pntMax.Y - pntMin.Y);
                       // ControlPaint.DrawReversibleFrame(rect, Color.Red, FrameStyle.Thick);
                        ControlPaint.FillReversibleRectangle(rect, Color.Red);
                        // ControlPaint.FillReversibleRectangle(rect, Color.Black);
                    }
                    break;


            }
            pxArea.Invalidate();
            tempPoint = e.Location;
        }

       
        
        
        private void PxArea_MouseDown(object? sender, MouseEventArgs e)
        {


            startPoint = e.Location;
         if (pntMode == "Pipetka")
            {
               clr =  b.GetPixel(e.Location.X, e.Location.Y);
                myPen.Color = clr;
                myBrush.Color = clr;
            }
         else
            bb = (Bitmap)b.Clone();
        }

        private void fmWfaPaint_Load(object sender, EventArgs e)
        {
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void btfColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                clr = colorDialog1.Color;
                myPen.Color = clr; myBrush.Color = clr;
                btColor.BackColor = clr;
            }
           // int x,y,height,width;
           // x = Program.fmWfaPaint.Left;
           // y = Program.fmWfaPaint.Top;
           // var rect = new Rectangle(x+100,y+100,40,40);
           //ControlPaint.DrawReversibleFrame(rect, Color.Black, FrameStyle.Thick);
           //// ControlPaint.FillReversibleRectangle(rect, Color.Black);
        }

        private void fmWfaPaint_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    b.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                }
                catch
                {
                    MessageBox.Show("Impossible to save image", "FATAL ERROR",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string _fn = openFileDialog1.FileName;
                    b = (Bitmap)Bitmap.FromFile(_fn);
                }
                catch
                {
                    MessageBox.Show("Impossible to save image", "FATAL ERROR",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void tbSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                    (e.KeyChar != '.'))
                {
                    e.Handled = true;
               
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
               
            }
            
        }

        private void tbSize_Click(object sender, EventArgs e)
        {

        }

        private void tbSize_TextChanged(object sender, EventArgs e)
        {
            _size = Convert.ToInt32(tbSize.Text);
            myPen.Width = _size;
        }
    }
}