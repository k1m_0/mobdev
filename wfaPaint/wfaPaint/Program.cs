namespace wfaPaint
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        /// 
        public static fmWfaPaint fmWfaPaint;


        [STAThread]
        static void Main()
        {

            ApplicationConfiguration.Initialize();
            Program.fmWfaPaint = new fmWfaPaint();
            Application.Run(Program.fmWfaPaint);
        }
    }
}