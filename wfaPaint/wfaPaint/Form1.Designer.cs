﻿namespace wfaPaint
{
    partial class fmWfaPaint
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmWfaPaint));
            this.pxArea = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.isFilled = new System.Windows.Forms.CheckBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btColor = new System.Windows.Forms.ToolStripButton();
            this.tbSize = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btPen = new System.Windows.Forms.ToolStripButton();
            this.btRect = new System.Windows.Forms.ToolStripButton();
            this.btElips = new System.Windows.Forms.ToolStripButton();
            this.btRevRect = new System.Windows.Forms.ToolStripButton();
            this.btPipetka = new System.Windows.Forms.ToolStripButton();
            this.btLine = new System.Windows.Forms.ToolStripButton();
            this.btArrow = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.pxArea)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pxArea
            // 
            this.pxArea.BackColor = System.Drawing.Color.White;
            this.pxArea.Location = new System.Drawing.Point(0, 28);
            this.pxArea.Name = "pxArea";
            this.pxArea.Size = new System.Drawing.Size(1061, 894);
            this.pxArea.TabIndex = 0;
            this.pxArea.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.splitter1);
            this.panel1.Location = new System.Drawing.Point(1061, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 894);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(46, 614);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Загрузить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(46, 570);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Сохранить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 894);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // isFilled
            // 
            this.isFilled.AutoSize = true;
            this.isFilled.Location = new System.Drawing.Point(446, 3);
            this.isFilled.Name = "isFilled";
            this.isFilled.Size = new System.Drawing.Size(74, 19);
            this.isFilled.TabIndex = 2;
            this.isFilled.Text = "Залитый";
            this.isFilled.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.toolStrip2);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Controls.Add(this.isFilled);
            this.panel2.Location = new System.Drawing.Point(0, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1264, 25);
            this.panel2.TabIndex = 5;
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btColor,
            this.tbSize});
            this.toolStrip2.Location = new System.Drawing.Point(222, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(221, 25);
            this.toolStrip2.TabIndex = 3;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btColor
            // 
            this.btColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btColor.Name = "btColor";
            this.btColor.Size = new System.Drawing.Size(23, 22);
            this.btColor.Text = "toolStripButton1";
            this.btColor.Click += new System.EventHandler(this.btfColor_Click);
            // 
            // tbSize
            // 
            this.tbSize.Name = "tbSize";
            this.tbSize.Size = new System.Drawing.Size(35, 25);
            this.tbSize.Text = "3";
            this.tbSize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSize_KeyPress);
            this.tbSize.Click += new System.EventHandler(this.tbSize_Click);
            this.tbSize.TextChanged += new System.EventHandler(this.tbSize_TextChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btPen,
            this.btRect,
            this.btElips,
            this.btRevRect,
            this.btPipetka,
            this.btLine,
            this.btArrow});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(222, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btPen
            // 
            this.btPen.CheckOnClick = true;
            this.btPen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btPen.Image = global::wfaPaint.Properties.Resources.Pen;
            this.btPen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btPen.Name = "btPen";
            this.btPen.Size = new System.Drawing.Size(23, 22);
            this.btPen.Text = "toolStripButton1";
            // 
            // btRect
            // 
            this.btRect.CheckOnClick = true;
            this.btRect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btRect.Image = global::wfaPaint.Properties.Resources.square;
            this.btRect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRect.Name = "btRect";
            this.btRect.Size = new System.Drawing.Size(23, 22);
            this.btRect.Text = "toolStripButton2";
            // 
            // btElips
            // 
            this.btElips.CheckOnClick = true;
            this.btElips.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btElips.Image = global::wfaPaint.Properties.Resources.ellips;
            this.btElips.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btElips.Name = "btElips";
            this.btElips.Size = new System.Drawing.Size(23, 22);
            this.btElips.Text = "toolStripButton3";
            // 
            // btRevRect
            // 
            this.btRevRect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btRevRect.Image = ((System.Drawing.Image)(resources.GetObject("btRevRect.Image")));
            this.btRevRect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRevRect.Name = "btRevRect";
            this.btRevRect.Size = new System.Drawing.Size(23, 22);
            this.btRevRect.Text = "toolStripButton1";
            // 
            // btPipetka
            // 
            this.btPipetka.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btPipetka.Image = global::wfaPaint.Properties.Resources.pipetka;
            this.btPipetka.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btPipetka.Name = "btPipetka";
            this.btPipetka.Size = new System.Drawing.Size(23, 22);
            this.btPipetka.Text = "toolStripButton1";
            // 
            // btLine
            // 
            this.btLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btLine.Image = global::wfaPaint.Properties.Resources._122;
            this.btLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btLine.Name = "btLine";
            this.btLine.Size = new System.Drawing.Size(23, 22);
            this.btLine.Text = "toolStripButton1";
            // 
            // btArrow
            // 
            this.btArrow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btArrow.Image = global::wfaPaint.Properties.Resources._123;
            this.btArrow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btArrow.Name = "btArrow";
            this.btArrow.Size = new System.Drawing.Size(23, 22);
            this.btArrow.Text = "toolStripButton1";
            // 
            // fmWfaPaint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 921);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pxArea);
            this.Name = "fmWfaPaint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "wfaPaint";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fmWfaPaint_FormClosed);
            this.Load += new System.EventHandler(this.fmWfaPaint_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pxArea)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox pxArea;
        private Panel panel1;
        private Splitter splitter1;
        private CheckBox isFilled;
        private ColorDialog colorDialog1;
        private SaveFileDialog saveFileDialog1;
        private Button button3;
        private Button button2;
        private OpenFileDialog openFileDialog1;
        private Panel panel2;
        private ToolStrip toolStrip1;
        private ToolStripButton btPen;
        private ToolStripButton btRect;
        private ToolStripButton btElips;
        private ToolStripButton btRevRect;
        private ToolStripButton btPipetka;
        private ToolStrip toolStrip2;
        private ToolStripButton btColor;
        private ToolStripButton btArrow;
        private ToolStripButton btLine;
        private ToolStripTextBox tbSize;
    }
}