﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfEventAttach
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAllClck(object sender, RoutedEventArgs e)
        {
            if (e.Source is Button x)
                lbLogs.Items.Add($"{sender.GetType()}");
        }

        private void RBClick(object sender, RoutedEventArgs e)
        {
            if (e.Source is Button x)
                lbLogs.Items.Add($"b:{x.Content}");
        }

        //private void ButtonAllclick(object sender, RoutedEventArgs e) {
        //    if (e.Source is Button x)
        //        lbLogs.Items.Add($"{sender.GetType()}");
        //}
        //private void RbAllClick(object sender, RoutedEventArgs e)
        //{
        //    if (e.Source is Button x)
        //        lbLogs.Items.Add($"{x.Content}");
        //}
    }
}
