﻿using System;

namespace consDrawRect
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isContinue = true;
          //  bool isField = true;
            do
            {
                int.TryParse(Console.ReadLine(), out int x);
                int.TryParse(Console.ReadLine(), out int y);
                Console.Write("Заливка? [Y/N] ->");
                if (Console.ReadLine().ToUpper() == "Y")
                {
                    for (int i = 0; i < y; i++)
                    {
                        Console.WriteLine(new String('*', x));
                    }
                }
                else
                {
                    Console.WriteLine(new String('*', x));
                    for (int i = 0; i < y - 2; i++)
                    {
                        Console.Write('*');
                        Console.Write(new String(' ', x - 2));
                        Console.WriteLine('*');
                    }
                    Console.WriteLine(new String('*', x));
                }

                Console.Write("Повторить? [Y/N] ->");
                isContinue = (Console.ReadLine().ToUpper() == "Y");
            }
            while (isContinue);
        }
    }
}
