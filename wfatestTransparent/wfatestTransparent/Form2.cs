﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfatestTransparent
{
    public partial class Form2 : Form
    {
        private Bitmap b;
        private Graphics g;
        private Random rnd = new Random();
        public Form2()
        {
            InitializeComponent();

            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);
           
            g = Graphics.FromImage(b);
            pictureBox1.Paint += (s, e) =>
            {
                e.Graphics.DrawImage(b, 0, 0);
            };
            timer1.Start();
        }
        char AsciiCharacters
        {
            get
            {
                int t = rnd.Next(90);
                string a = char.ConvertFromUtf32(12449 + t);
                return Convert.ToChar(a);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
           // g.Clear(Color.Wheat);
            g.FillRectangle(new SolidBrush(Color.Red),0, 0, 100, 100);
            bool isReadyAll = false;
         
          

            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    g.DrawString(AsciiCharacters.ToString(), new Font("Courier New", 10.0F, FontStyle.Bold), new SolidBrush(Color.White), new Point(12 * j, 12 * i));
                }
            }

            pictureBox1.Invalidate();
        }
    }
}
