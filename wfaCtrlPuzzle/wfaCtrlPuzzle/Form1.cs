namespace wfaCtrlPuzzle
{
    public partial class Form1 : Form
    {
        private const int Step = 25;
        private PictureBox[,] px;
        int CellWidth ;
        int CellHeight;
        private Point startPoint;
        private Random rnd =new Random();
        public int Rows { get; } = 3;
        public int Cols { get; } = 5;


        // �������� ��������� ������
        // ������� �������� ��� ������ �����
        public Form1()
        {
            InitializeComponent();
          

            CreateCell();
            ResizeCell();
            startLocationCells();

        }
        private void startLocationCells()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    px[i, j].Location = new Point( j * CellWidth, i * CellHeight);
                }
            }
        }
        private void CreateCell()
        {
            px = new PictureBox[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    px[i, j] = new PictureBox();
                    px[i, j].BorderStyle = BorderStyle.FixedSingle;
                    px[i, j].Tag = (i, j);
                    px[i, j].MouseDown += pictureB_mouseDown;
                    px[i, j].MouseMove += Form1_MouseMove;
                    px[i, j].MouseUp += Form1_MouseUp;
                    this.Controls.Add(px[i, j]);
                    this.KeyDown += Form1_KeyDown;
                    this.Text += ": (F1 - �������, F2 - ������ ������, F3 - ����������)";
                }
            }
        }

        private void Form1_KeyDown(object? sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
             
                case Keys.F1:
                    startLocationCells();
                    break;
                case Keys.F2:
                    ResizeCell();
                    break;
                case Keys.F3:
                    RandomLocationCells();
                    break;
                case Keys.F4:
                    RandomLocationCells2();
                    break;
                default:
                break;
            }
        }

        private void RandomLocationCells2()
        {
            startLocationCells();
            for (int i = 0; i < 10; i++)
            {
                var x1 = rnd.Next(Rows);
                var x2 = rnd.Next(Rows);
                var y1 = rnd.Next(Cols);
                var y2 = rnd.Next(Cols);


                var tempPoint = px[x1, y1].Location;
                px[x1, y1].Location = px[x2, y2].Location;

                px[x2, y2].Location = tempPoint;

            }
        }

        private void RandomLocationCells()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {

                    px[i, j].Location = new Point(rnd.Next(ClientSize.Width-px[i,j].Width), (rnd.Next(ClientSize.Height - px[i, j].Height)));
                }
            }
        }

        private void Form1_MouseUp(object? sender, MouseEventArgs e)
        {
            if (sender is Control c)
            {
                if (e.Button == MouseButtons.Left)
                {
                    var p = c.Location;
                    //

                    for (int i = 0; i < Rows; i++)
                    {
                        for (int j = 0; j < Cols; j++)
                        {
                            if (p.X> j*CellWidth- Step && p.X<j*CellWidth+ Step)
                            {
                                p.X = j * CellWidth;
                            }if (p.Y> i*CellHeight- Step && p.Y<i*CellHeight+ Step)
                            {
                                p.Y = i * CellHeight;
                            }
                        }
                    }
                    c.Location = p;
                    
                }
                    c.Cursor = Cursors.Default;
            }
        }

        private void Form1_MouseMove(object? sender, MouseEventArgs e)
        {
            if(sender is Control c)
            {
                if (e.Button == MouseButtons.Left)
                {
                    c.Cursor = Cursors.SizeAll;
                    c.Location = new Point(c.Location.X+ e.X - startPoint.X, c.Location.Y + e.Y - startPoint.Y);
                }
            
            (var row, var col) = ((int,int))c.Tag;
                this.Text = $"{row}:{col}";
            }
        }

        private void pictureB_mouseDown(object? sender, MouseEventArgs e)
        {
            startPoint = e.Location;
            if (sender is Control c)
            {
                c.BringToFront();
                c.Cursor = Cursors.SizeAll;
            }
        }

        private void ResizeCell()
        {
            
             CellWidth = this.ClientSize.Width / Cols;
           CellHeight = this.ClientSize.Height/ Rows;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    px[i, j].Width = CellWidth;
                    px[i, j].Height = CellHeight;
                    px[i, j].Image = new Bitmap(CellWidth, CellHeight);
                    var g = Graphics.FromImage(px[i, j].Image);
                    g.DrawImage(Properties.Resources.john,
                         new Rectangle(0, 0, CellWidth, CellHeight),
                         new Rectangle(j * CellWidth, i * CellHeight, CellWidth, CellHeight),
                         GraphicsUnit.Pixel);
                    g.DrawString($"[{i}{j}]", new Font("", 20), new SolidBrush(Color.Black), new Rectangle(0, 0, CellWidth, CellHeight), new StringFormat
                        {Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center}
                );
                    g.Dispose();

                }
            }
        }
    }
}