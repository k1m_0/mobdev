﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTabCtrl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
     

        private void CommandBinding_cmdAddTab(object sender, ExecutedRoutedEventArgs e)
        {
            tbCtrl.Items.Add("0");
        }

        private void CommandBindingcmdDelTab(object sender, ExecutedRoutedEventArgs e)
        {
            tbCtrl.Items.RemoveAt(tbCtrl.SelectedIndex);
        }
    }
    public class MyCommands
    {
        public static RoutedCommand cmdAddTab { get; private set; } = new RoutedCommand("cmdAddTab", typeof(MainWindow));
        public static RoutedCommand cmdDeleteTab { get; private set; } = new RoutedCommand("cmdDeleteTab", typeof(MainWindow));
    }
}
