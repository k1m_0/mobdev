namespace WfaImageRotate
{
    public partial class Form1 : Form
    {
        private readonly Bitmap imHero;

        public Form1()
        {
            InitializeComponent();
            imHero = new Bitmap(Properties.Resources.maxresdefault);
            trackBar1.ValueChanged += (s, e) => pictureBox1.Invalidate();
            pictureBox1.Paint += pbPaint;
            button1.Click += (s, e) => { imHero.RotateFlip(RotateFlipType.RotateNoneFlipX); pictureBox1.Invalidate(); };
            button2.Click += (s, e) => { imHero.RotateFlip(RotateFlipType.RotateNoneFlipY); pictureBox1.Invalidate(); };
        }

        private void pbPaint(object? sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(imHero.Width / 2,imHero.Height/2) ;
            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.DrawImage(imHero, -imHero.Width / 2, -imHero.Height / 2);

           e.Graphics.TranslateTransform(-imHero.Width / 2, -imHero.Height / 2);

            // e.Graphics.DrawImage(imHero, 50, 50);
            //e.Graphics.DrawImage(imHero, -imHero.Width / 2, -imHero.Height / 2);
        }
    }
}