namespace wfaRowsDown
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        //string[] col= new string[10];
        string col;
        char[,] symb = new char[500,500];
        int areaSize=0;
        int fontSize =12;
        int _x = 0, _y = 0;
        Label[,] cols = new Label[200,200];
        System.Windows.Forms.Timer[] timers = new System.Windows.Forms.Timer[20];
        private int counter=0;
        private string srasfas;

        private Bitmap b;
        private Graphics g;


        public Form1()
        {
            InitializeComponent();
            areaSize = pictureBox1.Height / fontSize;

            //makeClmn();

            b = new Bitmap(pbDraw.Width , pbDraw.Height);
            g = Graphics.FromImage(b);

            // MakeColomn();

            for (int i = 0; i < 20; i++)
            {
                //timers[i] = new System.Windows.Forms.Timer();
                //timers[i].Tag = i;
                //timers[i].Tick += Form1_Tick;
                //timers[i].Interval = 1;
                //timers[i].Start();
            }
            timNew.Tick += TimNew_Tick;
            pbDraw.Paint += (s, e) =>
            {
                e.Graphics.DrawImage(b, 0, 0);
            };
        }

        private void TimNew_Tick(object? sender, EventArgs e)
        {
            g.Clear(Color.White);
            g.DrawString(srasfas, new Font("Courier New", 30.0F), new SolidBrush(Color.Red), new Point(10, counter*1));
            pbDraw.Invalidate();
            counter++;
        }
       


        private void makeClmn()
        {
            counter = 0;
            srasfas = Convert.ToString(AsciiCharacters);
           
                g.Clear(Color.White);
                g.DrawString(srasfas, new Font("Courier New", 30.0F), new SolidBrush(Color.Red), new Point(10, 10));
            pbDraw.Invalidate();
            timNew.Start();
        }

        private void Form1_Tick(object? sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.Timer timer) {
                for (int j = 200 - 1; j >= 0; j--)
                {
                    cols[(int)timer.Tag, j].Top += 10;

                } }
        }

        char AsciiCharacters
        {
            get
            {
                int t = rnd.Next(90);
                string a = char.ConvertFromUtf32(12449 + t);
                return Convert.ToChar(a);
            }
        }



        private void timer2_Tick(object sender, EventArgs e)
        {
            //for (int j = 200 - 1; j >= 0; j--)
            //{
            //    cols[1, j].Top += 5;

            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {


            //for (int j = 200 - 1; j >= 0; j--)
            //{
            //    cols[0, j].Top += 5;

            //}

        }

        //using (Graphics g = pictureBox1.CreateGraphics())
        //    {
        //        g.Clear(Color.Black);
        //        int x;
        //        var sBrush = new SolidBrush(Color.Green);
        //        Point tempP;

        //            //sBrush.Color = Color.Green;




        //    for (int i = 0; i < areaSize; i++)
        //    {
        //        for (int j = 0; j < areaSize; j++)
        //        {
        //            tempP = new Point(i*fontSize,j*fontSize );

        //            sBrush.Color = GetPictureARGB(1, 1);
        //            g.DrawString(Convert.ToString(symb[i,j]), new Font("Courier New", fontSize), sBrush, tempP);
        //           // g.DrawString(col.Substring(i, 1), new Font("Courier New", fontSize), sBrush, tempP);
        //        }
        //    }



        //    _y += fontSize;
        //    if (_y>pictureBox1.Height)
        //    {
        //        _y = 0;
        //    }
        //    }

        private Color GetPictureARGB(int x,int y)
        {
            Bitmap bmp = new Bitmap(pictureBox2.Image);
            int _Alp;
            _Alp = (bmp.GetPixel(x, y).B + bmp.GetPixel(x, y).G + bmp.GetPixel(x, y).R) / 3;
            _Alp = (_Alp > 160 && _Alp < 220) ? 220 : _Alp;
            //condition ? consequent : alternative
            Point tempP = new Point(x, y);
            //Color newC = Color.FromArgb(_Alp, 0, 170, 0);
            Color newC = Color.FromArgb(_Alp, bmp.GetPixel(x, y).R, bmp.GetPixel(x, y).G, bmp.GetPixel(x, y).B);
            var sBrush = new SolidBrush(newC);
            //  pictureBox1.Image = bmp;



            return Color.Green;
        }

        void MakeColomn()
        {
            //    col = "";

            //for (int i = 0; i < areaSize; i++)
            //{

            //        col+= AsciiCharacters;
            //}


            //for (int i = 0; i < 500; i++)
            //{
            //    for (int j = 0; j < 500; j++)
            //    {
            //        symb[i, j] = AsciiCharacters;
            //    }
            //}



            
            for (int i = 0; i < 200; i++)
            {
                for (int j = 0; j < 200; j++)
                {
                    cols[i,j] = new Label();
                    cols[i,j].Text = Convert.ToString(AsciiCharacters);
                }
            }
            Point tempP;
           
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 200; j++)
                {
                    tempP = new Point(i * fontSize, j * fontSize);
                    //cols[i, j].Location = tempP;

                    cols[i, j].Top = tempP.Y;
                    cols[i, j].Left = tempP.X;
                    cols[i, j].ForeColor = Color.Green;
                    Font font = new Font("ms mincho", 10f, FontStyle.Bold | FontStyle.Italic);
                    
                    cols[i, j].Font = font;
                    //Font("Microsoft Sans Serif", 12F, FontStyle.Bold | FontStyle.Italic | FontStyle.Underline)
                    cols[i, j].Width = fontSize;
                    cols[i, j].Height = fontSize;
                    cols[i, j].BackColor = Color.Black;
                    cols[i, j].BringToFront();
                    cols[i, j].TextAlign = ContentAlignment.MiddleCenter;

                    this.Controls.Add(cols[i, j]);
                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            makeClmn();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
            timer1.Start();
            timer2.Start();
        }
    }
}