
namespace WfaRoadBuild
{
    public partial class Form1 : Form
    {

        private int playMode;
        Graphics g;
        Bitmap b;
        private SolidBrush myBrush;
        private Pen myPen;

        public Form1()
        {
            myBrush = new SolidBrush(Color.Green);
            myPen = new Pen(Color.Green);
            InitializeComponent();
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g = Graphics.FromImage(b);

            pictureBox2.Click += pBclick;
            pictureBox3.Click += pBclick;
            pictureBox4.Click += pBclick;
            pictureBox5.Click += pBclick;
            pictureBox6.Click += pBclick;
            pictureBox7.Click += pBclick;
            pictureBox8.Click += pBclick;
            pictureBox9.Click += pBclick;
            pictureBox10.Click += pBclick;
            pictureBox11.Click += pBclick;
            pictureBox12.Click += pBclick;
            pictureBox13.Click += pBclick;

            pictureBox1.Paint += (s, e) =>
            {
                e.Graphics.DrawImage(b, 0, 0);
            };
        }

      

    private void pBclick(object? sender, EventArgs e)
        {
            var s = sender as PictureBox;
            playMode = Convert.ToInt32(s.Tag);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            // 16 �� 8 ����
            // g = Graphics.FromImage(pictureBox2.Image);
          
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            object uImg;
            uImg = pictureBox2.Image;
            switch (playMode)
            {
                case 1:
                    uImg = pictureBox2.Image;
                    break;
                case 2:
                    uImg = pictureBox3.Image;
                    break;
                case 3:
                    uImg = pictureBox4.Image;
                    break;
                case 4:
                    uImg = pictureBox5.Image;
                    break;
                case 5:
                    uImg = pictureBox6.Image;
                    break;
                case 6:
                    uImg = pictureBox7.Image;
                    break;
                case 7:
                    uImg = pictureBox8.Image;
                    break;
                case 8:
                    uImg = pictureBox9.Image;
                    break;
                case 9:
                    uImg = pictureBox10.Image;
                    break;
                case 10:
                    uImg = pictureBox11.Image;
                    break;
                case 11:
                    uImg = pictureBox12.Image;
                    break;
                case 12:
                    uImg = pictureBox13.Image;
                    break;
                default:
                    break;
            }

            Point loc = new Point();
            loc.X = e.Location.X - e.Location.X % 64;
            loc.Y = e.Location.Y - e.Location.Y % 64;
            //g.DrawImage(pictureBox2.Image, e.Location);
            g.DrawImage((Image)uImg, loc);

            pictureBox1.Invalidate();
        }
    }
}