﻿const int snX = 51;

int[,] snail = new int[snX, snX];


for (int i = 0; i < snX; i++)
{
    for (int j = 0; j < snX; j++)
    {
        snail[i, j] = 0;
    }
}

int k = snX;
bool first = true;



int pravo = 0;
int verh = 0;

while (k > 2)
{
    //вправо
    for (int i = pravo; i < k + pravo; i++)
    {
        snail[verh, i] = 1;

    }
    if (!first)
    {
        pravo += 2;
        k -= 2;
    }
    first = false;
    if (k < 2)
    {
        break;
    }
    //вниз
    for (int i = verh; i < k + verh; i++)
    {
        snail[i, snX - pravo - 1] = 1;
    }
    //влево
    for (int i = snX - pravo - 1; i > snX - k - pravo - 1; i--)
    {
        snail[k + verh - 1, i] = 1;
    }

    k -= 2;
    if (k < 2)
    {
        break;
    }
    //вверх
    for (int i = k + verh; i > verh + 1; i--)
    {
        snail[i, verh] = 1;

    }
    verh += 2;
}



for (int i = 0; i < snX; i++)
{
    for (int j = 0; j < snX; j++)
    {
        Console.Write(snail[i, j]);
    }
    Console.WriteLine();
}

for (int i = 0; i < snX; i++)
{
    for (int j = 0; j < snX; j++)
    {
        if (snail[i, j] == 1)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write("  ");
        }
        else
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("  ");

        }

    }
    Console.WriteLine();
}
Console.ResetColor();