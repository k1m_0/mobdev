﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfBuilding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Binding binding = new Binding();
            binding.ElementName = "edCity";
            binding.Path = new("Text");
            laCity.SetBinding(TextBlock.TextProperty, binding);
           
        }

        
    }
    public class Herro 
    {
        public string Name { get; set; }
        public string Clan { get; set; } = "none";
        public int Hp { get; set; } = 100;
    }
}
