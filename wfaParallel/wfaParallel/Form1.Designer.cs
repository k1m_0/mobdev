﻿namespace wfaParallel
{
    partial class wfaParallel
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buCreateFiles = new System.Windows.Forms.Button();
            this.buDeleteFiles = new System.Windows.Forms.Button();
            this.edLogs = new System.Windows.Forms.TextBox();
            this.edDirTemp = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buCreateFiles
            // 
            this.buCreateFiles.Location = new System.Drawing.Point(63, 94);
            this.buCreateFiles.Name = "buCreateFiles";
            this.buCreateFiles.Size = new System.Drawing.Size(256, 48);
            this.buCreateFiles.TabIndex = 0;
            this.buCreateFiles.Text = "button1";
            this.buCreateFiles.UseVisualStyleBackColor = true;
            // 
            // buDeleteFiles
            // 
            this.buDeleteFiles.Location = new System.Drawing.Point(325, 94);
            this.buDeleteFiles.Name = "buDeleteFiles";
            this.buDeleteFiles.Size = new System.Drawing.Size(233, 48);
            this.buDeleteFiles.TabIndex = 1;
            this.buDeleteFiles.Text = "button2";
            this.buDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // edLogs
            // 
            this.edLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edLogs.Location = new System.Drawing.Point(37, 168);
            this.edLogs.Multiline = true;
            this.edLogs.Name = "edLogs";
            this.edLogs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.edLogs.Size = new System.Drawing.Size(730, 261);
            this.edLogs.TabIndex = 2;
            // 
            // edDirTemp
            // 
            this.edDirTemp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edDirTemp.Location = new System.Drawing.Point(63, 31);
            this.edDirTemp.Name = "edDirTemp";
            this.edDirTemp.Size = new System.Drawing.Size(704, 23);
            this.edDirTemp.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(564, 94);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(203, 48);
            this.button3.TabIndex = 4;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // wfaParallel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.edDirTemp);
            this.Controls.Add(this.edLogs);
            this.Controls.Add(this.buDeleteFiles);
            this.Controls.Add(this.buCreateFiles);
            this.Name = "wfaParallel";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button buCreateFiles;
        private Button buDeleteFiles;
        private TextBox edLogs;
        private TextBox edDirTemp;
        private Button button3;
    }
}