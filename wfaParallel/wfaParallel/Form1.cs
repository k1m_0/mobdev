namespace wfaParallel
{
    public partial class wfaParallel : Form
    {
        public int COUNT_FILES { get; private set; } = 100;

        public wfaParallel()
        {
            InitializeComponent();
            edDirTemp.Text = Path.Combine(Path.GetTempPath(),Application.ProductName);
            Directory.CreateDirectory(edDirTemp.Text);
            buCreateFiles.Click += BuCreateFiles_Click;
            buDeleteFiles.Click += BuDeleteFiles_Click;
            button3.Click += Button3_Click;
        }

        private void Button3_Click(object? sender, EventArgs e)
        {
            Parallel.Invoke(
                () => {/*������1*/ },
                () => {/*������2*/ },
                () => {/*������3*/ },
                () => {/*������4*/ },
                () => {/*������5*/ }
                );

        }

        private void BuDeleteFiles_Click(object? sender, EventArgs e)
        {
            Parallel.For(0, COUNT_FILES,
                (v) => File.Delete(Path.Combine(edDirTemp.Text, $"����_{v:000}.txt"))
                );
        }

       

        private void BuCreateFiles_Click(object? sender, EventArgs e)
        {
            Parallel.For(0, COUNT_FILES, CreateFile);
        }

        private void CreateFile(int v, ParallelLoopState arg2)
        {
            var fileName = $"����_{v:000}.txt";
            File.Create(Path.Combine(edDirTemp.Text, fileName)).Close();
            //edLogs.Text += $" ���� [{fileName}] ������ \n";
                
        }
    }
}