using System.Text;

namespace WfPersonal
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        char[] katakana = new char[90];
        int speed = 3;
        string unicode;
        Point pnt = new Point();
        string endText = "k1moRNG";


        static Color baseColor = Color.DarkGreen;
        static Color greenColor = Color.Green;
        static Color fadedColor = Color.White;
        static int flowspeed = 100;
        static int fastflow = flowspeed + 30;
        static int textflow = fastflow + 50;
        int Counter;
        static Random randomPosition = new Random();
        int width, height;
        int[] y;
         
        public Form1()
        {
            InitializeComponent();

            button1.Click += new EventHandler(button1_Click);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(pictureBox2.Image);
         
            int _Alp;
           
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
   
                    if (x % 8 == 0 && y % 8 == 0)
                    {
                        _Alp = (bmp.GetPixel(x, y).B + bmp.GetPixel(x, y).G + bmp.GetPixel(x, y).R) /3;
                        _Alp = (_Alp > 160 && _Alp < 220) ? 220 : _Alp;
                            //condition ? consequent : alternative
                        Point tempP = new Point(x, y);
                        Color newC = Color.FromArgb(_Alp, 0, 170, 0);
                        //Color newC = Color.FromArgb(_Alp, bmp.GetPixel(x, y).R, bmp.GetPixel(x, y).G, bmp.GetPixel(x, y).B);
                        var sBrush = new SolidBrush(newC);
                        //  pictureBox1.Image = bmp;

                        using (Graphics g = pictureBox1.CreateGraphics())
                        {
                            g.DrawString(Convert.ToString(AsciiCharacters), new Font("Courier New", 8.0F), sBrush, tempP);
                        }
                    }


                    //bmp.GetPixel(x, y) = Color.FromArgb(clold.A, clold.R / 33, clold.G / 33, clold.B / 33);

                }
                //pictureBox1.Image = bmp;
            }
        }

        void button1_Click(object sender, EventArgs e)

        {
           Initialize(out width, out height, out y);

            timer1.Enabled = true;
       }
      
        void Initialize(out int width, out int height, out int[] y)
        {
            height = pictureBox1.Size.Height;
            width= pictureBox1.Size.Width;
            y = new int[width];
            using (Graphics g = pictureBox1.CreateGraphics())
            {
                pictureBox1.BackColor = Color.Black;
            }
            for (int x = 0; x < width/12; ++x) {
                y[x] = randomPosition.Next(height);
                int asfarf = y[x];
            }
                  
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            Counter++;
            ColumnUpdate(width, height, y);
         }

         char AsciiCharacters
        {
            get
            {
                int t = randomPosition.Next(90);
                string a = char.ConvertFromUtf32(12449 + t);
                return Convert.ToChar(a);
            }
        }
         int YPositionFields(int yposition, int height)
        {
            if (yposition < 0) return yposition + height;
            else if (yposition < height) return yposition;
            else return 0;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        void ColumnUpdate(int width, int height, int[] y)
        {
            using (Graphics g = pictureBox1.CreateGraphics())
            {
                int x;
                var sBrush = new SolidBrush(Color.Green);
                Point tempP;
               
                    for (x = 0; x < width/14; ++x)
                    {

                        sBrush.Color = Color.Green;
                        tempP = new Point(x*14, y[x]*12);
                    
                        g.DrawString( Convert.ToString(AsciiCharacters), new Font("Courier New", 10.0F), sBrush, tempP);

                    //int temp = y[x] - 2*12;
                    //tempP = new Point(x*14, YPositionFields(temp, height)*12);
                    //g.DrawString(Convert.ToString(AsciiCharacters), new Font("Courier New", 10.0F), sBrush, tempP);
                   
                    
                    //sBrush.Color = Color.Black;
                    //int temp1 = y[x] - 20*12;
                   
                    //tempP = new Point(x*14-2, YPositionFields(temp1, height)*12);
                    //string clearer = "\u25a0";
                    //g.DrawString(clearer, new Font("Courier New", 18.0F), sBrush, tempP);

                    y[x] = YPositionFields(y[x] + 1, height);

                    }
            
            }

        }

    }
}