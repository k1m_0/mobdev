﻿using System;

namespace rand
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Write("введите количество строк : ");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.Write("введите количество столбцов : ");
            int m = Convert.ToInt32(Console.ReadLine());
            Random r = new Random();
            int[,] mas = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = r.Next(20)-10;
                    if (mas[i, j] > 0)
                        Console.BackgroundColor = ConsoleColor.Green;
                    else if (mas[i, j] == 0)
                        Console.BackgroundColor = ConsoleColor.Cyan;
                    else
                        Console.BackgroundColor = ConsoleColor.Red;

                    Console.Write("{0,4}",mas[i, j]);
                    Console.ResetColor();
                }
                Console.WriteLine();
                
            }
        }
    }
}
